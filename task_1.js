const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const getTimeSpentOnFrontendProblems = tasks => {
    let filteredTasks = tasks.filter(task => task.category === 'Frontend');
    let timeSpent = filteredTasks.reduce((sum, current) => sum + current.timeSpent, 0);
    console.log(timeSpent + ' time spent on Frontend problems');
};
getTimeSpentOnFrontendProblems(tasks);


const getTimeSpentOnBugTypeProblems = tasks => {
    let filteredTasks = tasks.filter(task => task.type === 'bug');
    let timeSpent = filteredTasks.reduce((sum, current) => sum + current.timeSpent, 0);
    console.log(timeSpent + ' time spent on Bug typed problems');
};
getTimeSpentOnBugTypeProblems(tasks);


const getNumOfTasksWithUI = tasks => {
    let filteredTasks = tasks.filter(task => task.title.indexOf('UI') !== -1);
    console.log(filteredTasks.length + ' tasks with UI in their title');
};
getNumOfTasksWithUI(tasks);


const getFrontBackObject = tasks => {
    let filteredTasks = tasks.reduce((container, task) => {
        task.category === 'Frontend' ? container.Frontend++ : container.Backend++;
        return container;
    }, {Frontend: 0, Backend: 0});
    console.log(filteredTasks , "Object with num of each category");
};
getFrontBackObject(tasks);


const getObjectWithTimeMoreThanFour = tasks => {
    let filteredTasks = tasks.filter(task => task.timeSpent > 4);
    filteredTasks = filteredTasks.reduce((container, task, index) => {
        container[index] = {title: task.title, category: task.category};
        return container;
    }, []);
    console.log(filteredTasks, "Object with timeSpent > 4 tasks");
};
getObjectWithTimeMoreThanFour(tasks);
