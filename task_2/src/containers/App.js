import React, {Component} from 'react';
import './App.css';
import InputField from '../components/InputField/InputField';
import ItemsField from "../components/ItemsField/ItemsField";
import Wrapper from "../hoc/Wrapper";

class App extends Component {
    state = {
        items: [],
        currentItemName: '',
        currentItemCost: ''

    };

    removeItem = index => {
        let items = [...this.state.items];
        const ind = items.findIndex(item => item.index === index);
        items.splice(ind, 1);
        this.setState({items});
    };

    saveCurrentItemName = (event) => {
        const state = {...this.state};
        state.currentItemName = event.target.value;
        this.setState(state);
    };

    saveCurrentItemCost = (event) => {
        const state = {...this.state};
        state.currentItemCost = parseInt(event.target.value, 10);
        this.setState(state);
    };

    addItemToBill = () => {
        const state = {...this.state};
        const name = state.currentItemName;
        const cost = state.currentItemCost;
        if(name.length !== 0 && cost.length !== 0){
            state.items.push({name, cost, index: Date.now()});
            state.currentItemName = '';
            state.currentItemCost = '';
            this.setState(state);
        }

    };

    countTotalSpent = () => {
        return this.state.items.reduce((sum, currentItem) => sum + currentItem.cost, 0);
    }


    render() {
        return (
            <Wrapper>
                <InputField
                    nameValue={this.state.currentItemName}
                    costValue={this.state.currentItemCost}
                    currentItemName={event => this.saveCurrentItemName(event)}
                    currentItemCost={event => this.saveCurrentItemCost(event)}
                    addItemToBill={this.addItemToBill}
                />
                <ItemsField
                    items={this.state.items}
                    removeItem={this.removeItem}
                    totalSpent={this.countTotalSpent()}
                />
            </Wrapper>
        );
    }
}

export default App;
