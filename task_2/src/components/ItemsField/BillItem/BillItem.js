import React from 'react';
import './BillItem.css';

const BillItem = props => {
    return (
        <div className="BillItem clearfix">
            <p className="BillItemName">{props.name}</p>
            <div className="BillItemCostAndDeleteBtn">
                <span className="BillItemCost">  {props.cost} KGZ</span>
                <button className="BillItemDeleteBtn" onClick={props.removeItem}><i className="fas fa-trash-alt"/></button>
            </div>

        </div>
    )
};

export default BillItem;