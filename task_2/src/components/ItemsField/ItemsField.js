import React from 'react';
import BillItem from "./BillItem/BillItem";
import './ItemsField.css';

const ItemsField = props => {
    let index = 0;
    return (
        <div className="ItemsFieldWrapper">
            <div className="ItemsField">
                {props.items.map(item => {
                    index++;
                    return (
                        <BillItem
                            name={item.name}
                            cost={item.cost}
                            removeItem={() => props.removeItem(item.index)}
                            key={index}
                        />)}
                )
                }
                <p className="TotalSpent">Total spent: {props.totalSpent} KGZ</p>
            </div>
        </div>

    )
};

export default ItemsField;
