import React from 'react';
import './InputField.css';

const InputField = props => {
    return (
        <div className="InputFieldWrapper">
            <div className="InputField">
                <input type="text" className='inputItemName' placeholder='Item name' onChange={props.currentItemName} value={props.nameValue}/>
                <input type="text" className="inputItemCost" id='cost' placeholder='Cost' onChange={props.currentItemCost} value={props.costValue}/>
                <label htmlFor="cost">KGZ</label>
                <button className="AddItemBtn" onClick={props.addItemToBill}>Add</button>
            </div>
        </div>
    )
};

export default InputField;